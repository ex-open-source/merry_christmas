defmodule MerryChristmas.Mixfile do
  use Mix.Project

  def application, do: [applications: [:httpoison]]

  def project do
    [
      app: :merry_christmas,
      deps: [
        {:ex_doc, ">= 0.0.0", only: :dev},
        {:floki, "~> 0.11.0"},
        {:httpoison, "~> 0.10.0"},
      ],
      description: "Simple christmas app and library.",
      docs: [extras: ["README.md"]],
      elixir: "~> 1.3",
      escript: [main_module: MerryChristmas],
      package: [
        files: ["lib/merry_christmas.ex", "mix.exs", "LICENSE", "README.md"],
        licenses: ["Apache 2.0"],
        links: %{"GitLab" => "https://gitlab.com/ex-open-source/merry_christmas"},
        maintainers: ["Tomassz Marek Sulima"],
        name: :merry_christmas,
      ],
      version: "0.1.0",
    ]
  end
end
