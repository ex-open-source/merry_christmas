# Merry Christmas!

[Merry Christmas](https://hex.pm/packages/merry_christmas) is a simple christmas app and library.

Use git to fetch this repo like: `git clone repo_url`.
Then compile app by running: `mix escript.build`.
Finally run it like:
`./merry_christmas josé_valim` or `./merry_christmas "José Valim"`.

or:

Add `merry_christmas` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    # your other deps here ...
    {:merry_christmas, "~> 0.1.0"},
  ]
end
```

and use it like:

```elixir
name = "José Valim"
MerryChristmas.wishes_for(name)

# your url and selector:
url = "url_to_page_with_wishes"
selector = "selector_to_fetch_wishes"
MerryChristmas.wishes_for(name, url, selector)

# your wishes:
wishes = ["Dear #{name}", "I wish you Merry Christmas!"]
MerryChristmas.print_wishes(wishes)

# custom joiner:
MerryChristmas.print_wishes(wishes, "\n\n🎁🎁🎁🎁🎁🎁🎁\n\n")
```

Merry Christmas! Ho! Ho! Ho!
