defmodule MerryChristmas do
  @moduledoc """
  Simple Christmas app and library.

  Use git to fetch this repo like: `git clone repo_url`.
  Fetch deps by: `mix deps.get`.
  Then compile app by running: `mix escript.build`.
  Finally run it like:
  `./merry_christmas josé_valim` or `./merry_christmas "José Valim"`.

  or:

  Add `merry_christmas` to your list of dependencies in `mix.exs`:

  ```elixir
  def deps do
    [
      # your other deps here ...
      {:merry_christmas, "~> 0.1.0"},
    ]
  end
  ```

  and use method from this module.
  """

  @default_selector "li"
  @default_url "http://www.my-best-wishes.com/holidays/christmas-wishes.php"

  @doc """
  Main method called when binary is runned
  If you pass only one argument (your name) then this app will print wishes.
  Otherwise this app will print it's usage.
  """
  def main([name]), do: wishes_for(name, @default_url, @default_selector)
  def main(_), do: IO.puts "Usage:\nmerry_christmas \"Your name\""

  @doc """
  Pass your wishes in `List`.
  This method will join them by joiner and print to console.

  Example:

      name = "José Valim"
      wishes = ["Dear \#{name}", "I wish you Merry Christmas!"]
      MerryChristmas.print_wishes(wishes)
      # or:
      MerryChristmas.print_wishes(wishes, "\\n\\n🎁🎁🎁🎁🎁🎁🎁\\n\\n")
  """
  def print_wishes(list, joiner \\ "\n\n") when is_list(list) do
    list
    |> Enum.join(joiner)
    |> IO.puts
  end

  @doc """
  This method fetch (by send GET request) page using url.
  Then it looks for elements matching selector gets random element of that list.
  Finally method call `&print_wishes/2` with greating, random wish and my wish.

  Example:

      name = "José Valim"
      MerryChristmas.wishes_for(name)

      # your url and selector:
      url = "url_to_page_with_wishes"
      selector = "selector_to_fetch_wishes"
      MerryChristmas.wishes_for(name, url, selector)
  """
  def wishes_for(name, url \\ @default_url, selector \\ @default_selector) do
    name = if String.contains?(name, "_"), do: format_name(name), else: name
    wish =
      HTTPoison.get!(url).body
      |> Floki.find(selector)
      |> Enum.random
      |> Floki.text
    greeting = "Dear #{name}!"
    random_wish = inspect(wish) <> "\n" <> "Source: #{url}"
    wish = "Merry Christmas and a Happy New Year for all Elixir developers!"
    print_wishes([greeting, random_wish, wish])
  end

  defp format_name(name) do
    name = Regex.replace(~r/_[^_ ]+/, name, &do_format_name/1)
    Regex.replace(~r/^[^_ ]+/, name, &String.capitalize/1)
  end

  defp do_format_name("_" <> match), do: " " <> String.capitalize(match)
end
